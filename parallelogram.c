/* Week 3 ex 9 */
/* Sharon Mafgaoker */
#include <stdio.h>
#define A 26

int main(void)
{
    int n,i,j,x,symbol,space;
    printf("Enter a number between 1 - 26: ");
    scanf("%d", &n);
    symbol = 65;
    space  = 32; 
    if((n>=1) && (n<=A)){
        for(i=0; i<n ; i++){
            for(j=0; j<n ; j++){
                printf("%c", symbol);
            }
            printf("\n");
            for(x=0; x<=i; x++){
                printf("%c", space);
            }
            symbol++;
        }
    }
    else{
        printf("Size input error.\n");
    }

    return 0;

}
