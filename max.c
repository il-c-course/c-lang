#include <stdio.h>

int main(void){

    double a, b, c, d;
    double max;

    printf("Enter 4 numbers: ");
    scanf("%lf %lf %lf %lf", &a, &b, &c , &d);

    if(a>b){
        max=a;
    }
    else{
        max=b;
    }
    if(c>max){
        max=c;
    }
    if(d>max){
        max=d;
    }
    printf("The max number is %lf\n", max);
    return 0;
}
