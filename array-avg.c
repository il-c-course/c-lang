#include <stdio.h>
#define N 4
int main(void) {
    
    int a[N];
    int i;
    int sum, count;

    double avg;

    printf("Enter the numbers: ");
    for (i=0; i<N; i++ ){
        scanf("%d", &a[i]);
    }
    sum=0;
    for(i=0; i<N; i++){
        sum+=a[i];
    }
    avg=(double)sum/N;
    count=0;
    for(i=0; i<N; i++){
        if(a[i]>avg){
            count++;
        }
    }
    printf("There are %d numbers above the average\n", count);
    return 0;

}
