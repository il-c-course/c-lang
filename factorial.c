#include <stdio.h>

int main(void)
{
    int n, factorial, i;
    printf("Enter a positvie number: ");
    scanf("%d", &n);
    
    if(n>0)
    {
        factorial=1;
        for(i=1; i<=n ; i++)
        {
            factorial*=i;
        }
        printf("%d! = %d\n", n, factorial);
    }
    else
    {
        printf("positvie number only\n");
    }
    return 0;
}
