#include <stdio.h>
#define NUM_STUDENTS 10
#define MIN_GRADE 200
#define MAX_GRADE 800

int main(void)
{
    int i;
    int grades[NUM_STUDENTS];
    int hist[MAX_GRADE - MIN_GRADE +1] = {0};

    for(i=0; i<NUM_STUDENTS; i++)
    {
        scanf("%d", &grades[i]);
    }
    for(i=0; i<NUM_STUDENTS; i++)
    {
        int g = grades[i];
        hist[g-MIN_GRADE]++;
    }
    for(i=0; i<(MAX_GRADE-MIN_GRADE+1); i++ )
    {
        
        printf("%d", hist[i]);
        
    }
    
    return 0;
} 
