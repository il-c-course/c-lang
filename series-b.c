/* Week 3 ex 7 */
/* Sharon Mafgaoker  */
#include <stdio.h>

int main(void)
{
    int n,a ;

    printf("Enter series of numbers: ");
    scanf("%d", &n);
    a=n;
    while (n>0)
    {
        printf("a = %d\n", a);
        if (n<=a)
        {
            a=n;
        }
        scanf("%d", &n);
    }
    printf("\n%d\n", a);
    return 0;
}
