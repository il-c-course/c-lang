#include <stdio.h>
#define NUM_STUDENTS 10

int main(void)
{
    int i;
    int grades[NUM_STUDENTS];
    int hist[101] = {0};

    for(i=0; i<NUM_STUDENTS; i++)
    {
        scanf("%d", &grades[i]);
    }
    for(i=0; i<NUM_STUDENTS; i++)
    {
        int g = grades[i];
        hist[g]++;
    }
    for(i=0; i<101; i++ )
    {
        if(hist[i]>0){
            printf("%d", hist[i]);
        }
        
    }
    
    return 0;
} 
