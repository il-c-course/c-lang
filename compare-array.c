/* Week 4 ex 2  */
/* Sharon Mafgaoker */
#include <stdio.h>
#define SIZE 10


int main(void)
{
    int i,j,count,nA,nB = 0;
    char A[SIZE+1] = {0};
    char B[SIZE+1] = {0};
    char temp;
    printf("Enter a number between 1 - 10 to represent the size of the first array\n");
    scanf("%d",&nA);
    printf("Enter %d characters\n", nA);
    scanf("%c",&temp);
    
    scanf("%[^\n]s",A);

    printf("Enter a number between 1 - 10 to represent the size of the second array\n");
    scanf("%d", &nB);
    printf("Enter %d characters\n", nB);
    scanf("%c",&temp);
    
    scanf("%[^\n]s",B);
    
    i=0;
    for(j=0; j<nA; j++)
    {
        if(B[i] == A[j])
        {
            printf("i = %d, j = %d, %c = %c, count = %d\n",i,j, B[i], A[j], count);
            i++;
            count++;
        }else if(count == nB)
        {
            break;
        }
        else
        {
            i=0;
            count=0;
        }
    }
   
    if(count == nB){
        printf("The second array is within the first array\n");
    }
    return 0;
}
