/* Week 3 ex 8 */
/* Sharon Mafgaoker */
#include <stdio.h>

int main(void)
{
    int n,digit;
    printf("Enter a positive number: ");
    scanf("%d", &n);
    
    if(n>0)
    {
    while(n)
    {
        digit = n % 10;
        printf("%d",digit);
        n /= 10;
    }
    printf("\n");
    }
    else{
        printf("The program except only positive numbers\n");
    }
    return 0;
}
