#include <stdio.h>

int main(void)
{
    int i,n,sum;
    sum=0;
    printf("Enter the numbers:\n");
    for(i=0; i<10; i++){
        scanf("%d", &n);
        if(n<=0)
        {
            continue;
        }
        sum+=n;
    }
    printf("The sum of the numbers is %d\n", sum);
    return 0;
}
