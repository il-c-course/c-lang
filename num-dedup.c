/* Week 4 ex 3  */
/* Sharon Mafgaoker */

#include <stdio.h>
#define NUM 10

int main(void)
{
    int i,j,n = 0;
    int numbers[NUM] = {0};
    int uniq[NUM] = {0};

    printf("Enter %d Numbers: \n", NUM);
    for(i=0; i<NUM; i++)
    {
        scanf("%d", &numbers[i]);
    }
    printf("\n");

    for(i=0; i<NUM; i++)
    {
        for(j=i+1; j<NUM; j++)
        {
            if(numbers[i] == numbers[j])
            {   
                uniq[j] = j;
            }
        }
        printf("%d ", numbers[i]);
    }
    printf("\n");
    for(i=0; i<NUM; i++)
    {
        if(uniq[i] == 0)
        {
            printf("%d ", numbers[i]);
        }
    }
    printf("\n");
    return 0;
}
