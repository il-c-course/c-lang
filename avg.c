#include <stdio.h>

int main(void)
{
    double x, sum, avg;
    sum = 0;
    int c = 0;
    int n;

    printf("Enter the number for the average you need: ");
    scanf("%d", &n);
    
    while (c<n)
    {
        scanf("%lf", &x);
        sum +=x;
        c++;
    }
    
    avg=sum/n;
    printf("the average is: %f\n", avg);

    return 0;
}
