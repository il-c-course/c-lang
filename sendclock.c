/* Week 3 ex 10 */
/* Sharon Mafgaoker */
#include <stdio.h>

int main(void)
{
    char c,clower;
    int n,i,j,x,space,cnspace;
    n = i = x = j = cnspace = 0;
    printf("Enter a character between A - Z and a odd numbers 1 - 21: ");
    scanf("%c %d", &c, &n);
    clower = c + 32;
    space  = 32; 
    if((c<65) || (c>90)){
        printf("Letter input error\n");
        return 0;
    }
    else if((n<1) || (n>21)){
        printf("Size input error.\n");
        return 0;
    }
    else if((n%2)==0){
        printf("Size is not odd.. = %d \n", n);
        return 0;
    }
    printf("Char = %c, lower = %c, digit = %d\n", c, clower, n);

    for(i=n; i>0; i--){
        if(i%2==1){
            for(j=0; j<i ; j++){
                printf("%c", c);
            }
            printf("\n");
            for(x=0; x<=cnspace; x++){
                if(i>1){
                    printf("%c", space);
                }  
            }
            cnspace++;
        }
    }
    for(i=0; i<=n; i++){
        if(i%2==1){
            cnspace--;
            for(x=0; x<cnspace; x++){
                    printf("%c", space);
            }
            for(j=0; j<i ; j++){
                printf("%c", clower);
            }
            printf("\n");
        }
    }
    return 0;
}
