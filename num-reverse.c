/* Week 4 ex 1  */
/* Sharon Mafgaoker */

#include <stdio.h>
#define NUM 10

int main(void)
{
    int i;
    int numbers[NUM] = {0};
    printf("Enter %d Numbers: ", NUM);
    for(i=0; i<NUM; i++)
    {
        scanf("%d", &numbers[i]);
    }
    printf("\n");
    for(i=(NUM-1); i>=0; i--)
    {
        printf("%d ", numbers[i]);
    }
    return 0;
}
