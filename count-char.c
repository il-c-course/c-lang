/* Week 4 ex 5  */
/* Sharon Mafgaoker */

#include <stdio.h>
#define CHAR 10
#define a_z 123

int main(void)
{
    int i,j = 0;
    char latters[CHAR+1] = {""};
    int count[a_z] = {0};

    printf("Enter %d characters\n", CHAR);
    scanf("%s", latters);
    
    for(i=0; i<CHAR; i++)
    {
        j = latters[i];
        if((j >=97) && (j <=122))
        {
             count[j]++;
        }
    }
    printf("\n");

    for(i=97; i<a_z; i++)
    {
        if(count[i] > 0)
        {
            printf("%c appears %d times \n", i, count[i]);
        }
    }
    return 0;
}
