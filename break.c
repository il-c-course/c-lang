#include <stdio.h>

int main(void)
{
    int n, sum;
    sum = 0;

    printf("Enter the numbers:\n");
    while (1)
    {
        scanf("%d", &n);
        if(n==0){
            break;
        }
        sum+=n;
    }
    printf("The sum of the numbers is %d\n", sum);
    return 0;
    
}
