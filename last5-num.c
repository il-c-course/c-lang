/* Week 4 ex 7  */
/* This program will print the last 5 number by they order  */
/* Sharon Mafgaoker */

#include <stdio.h>
#define CAP 100000

int main(void)
{
    int n,i,j = 0;
    int numbers[CAP] = {0};
    
    printf("Enter the numbers:\n");
    i=0;
    n=0;
    do
    {
        scanf("%d", &n);
        numbers[i] = n;
        i++;
        if(i==CAP-1)
        {
            n=0;
        }
    }while (n!=0 );

    for(j=i-6; j<i-1; j++ )
    {
        printf(" %d", numbers[j]);
    }
    printf("\n");
    return 0;
}
