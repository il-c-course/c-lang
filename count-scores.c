/* Week 4 ex 4  */
/* Sharon Mafgaoker */

#include <stdio.h>
#define NUM 10
#define MAX_SCORES 101

int main(void)
{
    int i,j = 0;
    int scores[NUM] = {0};
    int count[MAX_SCORES] = {0};

    printf("Enter %d scores between  0...100: \n", NUM);
    for(i=0; i<NUM; i++)
    {
        scanf("%d", &scores[i]);
    }
    printf("\n");

    for(i=0; i<NUM; i++)
    {
        j = scores[i];
        count[j]++;
    }
    printf("\n");

    for(i=0; i<MAX_SCORES; i++)
    {
        if(count[i] > 0)
        {
            printf("%d appears %d times \n", i, count[i]);
        }
        
    }
    return 0;
}
