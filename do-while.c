#include <stdio.h>

int main(void)
{
    int n, sum;
    sum = 0;
    
    printf("Enter the numbers:\n");
    do
    {
        scanf("%d", &n);
        sum+=n;
    }while (n!=0);
    
    printf("The sum of the numbers is %d\n", sum);
    return 0;
}
