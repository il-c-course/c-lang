#include <stdio.h>
#include <math.h>

int main(void)
{
    double a, b, c;
    double x1, x2 ;
    double disc;

    printf("Enter the coefficeients (a b and c): ");
    scanf("%lf %lf %lf", &a, &b, &c);

    disc=b*b-4*a*c;
    
    if(disc >= 0){
        x1=(-b+sqrt(disc))/2*a;
        x2=(-b-sqrt(disc))/2*a;
        printf("The roots of the equation are: %f, %f\n", x1, x2);
    }
    else {
        printf("The equation does not have a real roots\n");
    }
    return 0;
}
