/* Week 3 ex 6 */
/* Sharon Mafgaoker */
#include <stdio.h>

int main(void)
{
    int n,i,j ;
    printf("Enter a positive number: ");
    scanf("%d", &n);
    
    if(n>=0)
    {
    for (i=0; i<n; n--)
    {
        for(j=0; j<n; j++)
        {
            printf("*");
        }
        printf("\n");
    }
    }
    else{
        printf("The program except only positive numbers\n");
    }
    return 0;
}
