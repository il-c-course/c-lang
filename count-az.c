/* Week 4 ex 6  */
/* This program will show you if the string include all the charfrom 
A to Z in capital. */
/* Sharon Mafgaoker */

#include <stdio.h>
#define CHAR 1000
#define A_Z 91

int main(void)
{
    int i,j,count = 0;
    char latters[CHAR] = {""};
    int az[A_Z] = {0};

    printf("Enter characters\n");
    scanf("%[^\n]s",latters);
    

    for(i=0; i<CHAR; i++)
    {
        j = latters[i];
        if(j == 48) //0
        {
            break;
        }
        else if((j >=65) && (j <=90))
        {
             az[j] = 1;
        }
    }
    printf("\n");

    for(i=65; i<A_Z; i++)
    {
        printf("%d = %d \n", i, az[i]);
    }

    for(i=65; i<A_Z; i++)
    {
        j = az[i];
        if(j == 1)
        {   
            count++;
        }
    }
    if(count == 26)
    {
        printf("All char from A to Z found.\n");
        return 0;
    }
    return 0;
}
